package forum

import forum.grails.LoginController
import forum.grails.User
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(LoginController)
class LoginControllerSpec extends Specification {

    def 'test přihlašování bez zadání uživ.jména'() {
        setup: 'nastavím HTTP parametry'
        params.username = ''
        params.password = 'password1'

        when: 'přihlásím se bez uživatelského jména'
        controller.login()

        then: 'redirect na stránku login s chybou v uživ. jméně'
        view == '/login/index'
        model.loginCo.errors.getFieldError('username')
    }

    def 'test přihlašování neexistujícího uživatele'() {
        setup: 'mock pro chybovou hlášku, která se vrátí při nenalezení uživatele'
        String notFoundMockMsg = "not found"
        messageSource.addMessage("user.not.found.message", request.locale, notFoundMockMsg)
        params.username = 'username'
        params.password = 'password1'

        when: 'přihlásím se s neexistujícím uživatelem'
        controller.login()

        then:
        response.redirectedUrl == '/login/index'
        flash.errorMessage == notFoundMockMsg
    }

    def 'test přihlašování s existujícím uživatelem'() {
        setup: 'vytvořím mock uživatele, který se vrátí při testu'
        User mockUser = new User(username: "username", password: "password1")
        User.metaClass.static.findByUsernameAndPassword = { username, password ->
            if (username == mockUser.username && password == mockUser.password) {
                return mockUser
            }
        }
        String successMockMsg = "success"
        messageSource.addMessage("user.login.success.message", request.locale, successMockMsg)

        when:
        params.username = 'username'
        params.password = 'password1'
        controller.login()

        then:
        session.user == mockUser
        flash.successMessage == successMockMsg
        response.redirectedUrl == "/home"
    }
}