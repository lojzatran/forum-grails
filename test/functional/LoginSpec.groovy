import geb.spock.GebReportingSpec

/**
 * Test prihlasovani uzivatele do aplikace
 *
 * @author Lojza
 */
class LoginSpec extends GebReportingSpec {

    def "přihlašování se jako běžný uživatel"() {
        setup: 'půjdu na stránku pro přihlašování a zadám uživ. jméno a heslo'
        go "login"
        $('#username') << "user"
        $('#password') << "user"

        when: ' přihlásím se'
        $('#login-submit').click()

        then: 'uvidím odkazy na vytvoření nového tématu a uživatelského profilu, ale ne do admin menu'
        $('#create-topic-link').size() == 1
        $('#user-profile-link').size() == 1
        $('#admin-menu-link').size() == 0
    }

    def "přihlašování se jako admin" () {
        setup: 'půjdu na stránku pro přihlašování a zadám uživ. jméno a heslo'
        go "login"
        $('#username') << "admin"
        $('#password') << "admin"

        when: ' přihlásím se'
        $('#login-submit').click()

        then: 'uvidím odkazy na vytvoření nového tématu a uživatelského profilu a admin menu'
        $('#create-topic-link').size() == 1
        $('#user-profile-link').size() == 1
        $('#admin-menu-link').size() == 1
    }
}
