import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.htmlunit.HtmlUnitDriver

/**
 * Konfigurace Geb
 *
 * @author Lojza
 */
reportsDir = "target/geb-reports"
baseUrl = "http://localhost:8080/"
reportOnTestFailureOnly = true

waiting {
    timeout = 10
    retryInterval = 0.5
}

//driver = { new FirefoxDriver() }


