import forum.grails.Role
import forum.grails.TagController
import grails.test.spock.IntegrationSpec
import org.springframework.http.HttpStatus

/**
 * Testy pro tridu TagController
 *
 * @author Lojza
 */
class TagControllerSpec extends IntegrationSpec {

    def "test hledání tagu bez přihlášení"() {
        setup:
        def tagController = new TagController()
        tagController.params.tagName = "test tag"

        when:
        tagController.exists()

        then: 'bez admin role můžeme jen vyhledat existující tagy'
        tagController.response.status == HttpStatus.NOT_FOUND.value()
        tagController.response.text == "Tag neexistuje, prosím vytvořte první."
    }

    def "test vytvoření tagu jako admin"() {
        setup:
        def session = [user: [:]]
        def tagController = new TagController()
        tagController.metaClass.getSession = {-> session }
        tagController.params.tagName = "test tag"
        tagController.session.user.role = Role.ROLE_ADMIN

        when: 'hledám neexistující tag'
        tagController.exists()

        then: 'protože jsem admin, tak tag se rovnou vytvoří'
        tagController.response.status == HttpStatus.CREATED.value()
        tagController.response.text.isInteger()

        when: 'hledám tag jako uživatel'
        tagController.session.user.role = Role.ROLE_USER
        tagController.exists()

        then:
        tagController.response.status == HttpStatus.OK.value()
        tagController.response.text.isInteger()
    }
}
