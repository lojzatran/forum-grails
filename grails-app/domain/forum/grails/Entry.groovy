package forum.grails

class Entry {

    String text
    User author
    Topic topic

    Date dateCreated
    Date lastUpdated

    static belongsTo = Topic

    static constraints = {
        text blank: false
    }

    static mapping = {
        text type: "text"
    }
}
