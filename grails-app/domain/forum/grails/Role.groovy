package forum.grails

enum Role {
    ROLE_ANONYMOUS, ROLE_USER, ROLE_ADMIN
}
