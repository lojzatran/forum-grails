package forum.grails

class Topic {

    String name
    String description

    User author
    List<Entry> entryList = [];
    Set<Tag> tagList = [] as Set;

    Date dateCreated
    Date lastUpdated

    static Map hasMany = [
            entryList: Entry,
            tagList: Tag
    ]

    static Closure mapping = {
        description type: "text"
        entryList sort: 'dateCreated'
    }
}
