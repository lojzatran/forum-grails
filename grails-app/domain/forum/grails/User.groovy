package forum.grails

class User {

    String username
    String password

    Profile profile = new Profile()
    Role role = Role.ROLE_USER

    List<Topic> myTopics
    List<Entry> myEntries

    static hasOne = [
            profile: Profile
    ]

    static hasMany = [
            myTopics: Topic,
            myEntries: Entry
    ]

    static constraints = {
        username unique: true
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        User user = (User) o

        if (username != user.username) return false

        return true
    }

    int hashCode() {
        return username.hashCode()
    }

    boolean isAdmin() {
        return role == Role.ROLE_ADMIN
    }
}
