package forum.grails

class Profile {

    String firstName
    String lastName
    String email
    String signature
    byte[] avatar

    User user

    static mapping = {
        id column: 'user_id', generator: 'foreign',
                params: [property: 'user']
        user insertable: false, updateable: false
    }

    static belongsTo = [
            user: User
    ]

    static constraints = {
        firstName nullable: true
        lastName nullable: true
        email nullable: false
        signature nullable: true
        avatar nullable: true
    }
}
