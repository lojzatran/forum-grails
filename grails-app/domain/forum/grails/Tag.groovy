package forum.grails

class Tag {

    String name

    static hasMany = [
            topicList: Topic
    ]

    static belongsTo = Topic

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Tag that = (Tag) o

        // unsaved objects are never equal
        if (!this.id || !that.id) {
            return false
        }

        return this.id == that.id
    }

    int hashCode() {
        return name.hashCode()
    }
}
