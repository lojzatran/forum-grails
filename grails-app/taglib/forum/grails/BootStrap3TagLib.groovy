package forum.grails

class BootStrap3TagLib {
    static namespace = "bootstrap3"
    static defaultEncodeAs = 'raw'

    /**
     * Vytvori textove pole se stylem BootStrap3
     * @attr model REQUIRED model
     * @attr field REQUIRED atribut, pro ktery bude vygenerovan tento prvek
     * @attr labelName atribut name pro label
     * @attr name atribut name pro textovy pole
     * @attr id unikatni id pro textove pole
     * @attr value hodnota textoveho pole
     * @attr placeholder placeholder pro text. pole
     * @attr help pomocny text
     */
    def textField = { attrs ->
        out << renderField("/bootstrap3/textfield", attrs)
    }

    /**
     * Vytvori pole na heslo se stylem BootStrap3
     * @attr model REQUIRED model
     * @attr field REQUIRED atribut, pro ktery bude vygenerovan tento prvek
     * @attr labelName atribut name pro label
     * @attr name atribut name pro heslo
     * @attr id unikatni id pro heslo
     * @attr value hodnota hesla
     * @attr placeholder placeholder pro heslo
     * @attr help pomocny text
     */
    def passwordField = { attrs ->
        out << renderField("/bootstrap3/password", attrs)
    }

    /**
     * Vytvori textarea se stylem BootStrap3
     * @attr model REQUIRED model
     * @attr field REQUIRED atribut, pro ktery bude vygenerovan tento prvek
     * @attr labelName atribut name pro label
     * @attr name atribut name pro heslo
     * @attr id unikatni id pro heslo
     * @attr value hodnota hesla
     * @attr placeholder placeholder pro heslo
     * @attr help pomocny text
     * @attr cols pocet sloupcu
     * @attr rows pocet radku
     */
    def textArea = { attrs ->
        out << renderField("/bootstrap3/textarea", attrs)
    }

    def tags = { attrs ->
        out << renderField("/bootstrap3/tags", attrs)
    }

    private renderField(String templateName, attrs) {
        def labelText = attrs.remove("labelName") ?: "element name"
        def field = attrs.remove("field") ?: ""
        def name = attrs.remove("name") ?: field
        def id = attrs.remove("id") ?: name
        def placeholder = attrs.remove("placeholder") ?: ""
        def help = attrs.remove("help")
        def model = attrs.remove("model")
        def specificAttrs = attrs

        g.render(template: templateName, model: [labelText: labelText, name: name, specificAttrs: specificAttrs,
                id: id, field: field, placeholder: placeholder, help: help, model: model])
    }
}
