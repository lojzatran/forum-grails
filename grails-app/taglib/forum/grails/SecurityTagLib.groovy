package forum.grails

/**
 * Metody pro zjisteni
 * totoznosti aktualne
 * prihlaseneho uzivatele
 *
 * @author Lojza
 */
class SecurityTagLib {
    static namespace = "sec"

    /**
     * Vytiskne obsah tagu pokud
     * je uzivatel prihlasen
     */
    def ifLoggedIn = { attrs, body ->
        if (session.user) {
            out << body()
        }
    }

    /**
     * Vytiskne obsah tagu pokud
     * uzivatel neni prihlasen
     */
    def ifNotLoggedIn = { attrs, body ->
        if (!session.user) {
            out << body()
        }
    }

    /**
     * Vytiskne obsah tagu pokud
     * uzivatel ma zadanou roli
     * @attr role nazev role
     */
    def hasRole = { attrs, body ->
        String roleName = attrs.role
        Role role = Enum.valueOf(Role.class, roleName)
        if (session.user?.role == role) {
            out << body()
        }
    }

    /**
     *  Vytiskne obsah tagu pokud
     *  uzivatel muze objekt editovat
     */
    def canEditItem = { attrs, body ->
        def item = attrs.item
        User author = item.author
        if (session.user && author && (author.equals(session.user) ||
                session.user.role == Role.ROLE_ADMIN)) {
            out << body()
        }
    }
}
