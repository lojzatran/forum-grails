<%--
  Created by IntelliJ IDEA.
  User: Lojza
  Date: 12.1.14
  Time: 10:25
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Login</title>
    <meta name="layout" content="main"/>
</head>

<body>

<div class="col-md-6 col-md-offset-3">
    <g:form name="loginForm" action="login" role="form" class="form-horizontal" method="post">
        <h1><g:message code="login.action.label" default="Přihlásit se"/></h1>
        <bootstrap3:textField model="${loginCo}" field="username" id="username"
                              labelName="Jméno" name="username"/>
        <bootstrap3:passwordField model="${loginCo}" field="password" id="password"
                             labelName="Heslo" name="password"/>
        <g:submitButton name="login-submit" class="btn btn-default" value="Login"/>
    </g:form>
</div>

</body>
</html>