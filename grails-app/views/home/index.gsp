<!DOCTYPE html>
<html>
<head>
    <title><g:message code="forum.brand" default="Fórum"/></title>
    <meta name="layout" content="main"/>
</head>

<body>

<h1><g:message code="forum.brand" default="Fórum"/></h1>

<h2><g:message code="topic.list" default="Seznam témat"/></h2>
<g:each in="${topicList}" var="topic">
    <blockquote>
        <p>
            <g:link controller="topic" action="show" id="${topic.id}">
                ${topic.name} (počet příspěvků: ${topic.getEntryList()?.size()})
            </g:link>
        </p>
        <small>Založil <cite title="Source Title">${topic.author.username}</cite></small>
    </blockquote>
</g:each>

<g:if test="${session.user}">
    <p>
        <g:link controller="topic" action="create" elementId="create-topic-link">Vytvořit nové téma</g:link>
    </p>
</g:if>

</body>
</html>