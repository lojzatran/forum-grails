<%--
  Created by IntelliJ IDEA.
  User: Lojza
  Date: 18.1.14
  Time: 11:16
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Upravit příspěvek ${entryInstance.id}</title>
    <meta name="layout" content="main"/>
</head>

<body>

<div class="row">
    <g:hasErrors bean="${entryInstance}">
        <span class="help-block">
            <g:renderErrors bean="${entryInstance}"/>
        </span>
    </g:hasErrors>

    <g:form method="put" resource="${entryInstance}" action="update">
        <fieldset>
            <legend>${entryInstance.id}</legend>
            <g:if test="${entryInstance.id}">
                <g:hiddenField name="version" value="${entryInstance.version}"/>
                <g:hiddenField name="id" value="${entryInstance.id}"/>
            </g:if>
            <g:render template="form" model="[entryInstance: entryInstance]"/>
        </fieldset>
        <g:actionSubmit value="Aktualizovat" action="update" type="submit" class="btn btn-primary"/>
    </g:form>
</div>

</body>
</html>