<%--
  Created by IntelliJ IDEA.
  User: Lojza
  Date: 18.1.14
  Time: 11:16
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Přidat příspěvek</title>
    <meta name="layout" content="main"/>
</head>

<body>

<div class="row">
    <g:hasErrors bean="${entryInstance}">
        <span class="help-block">
            <g:renderErrors bean="${entryInstance}"/>
        </span>
    </g:hasErrors>

    <g:form method="post" resource="${entryInstance}" action="save">
        <fieldset>
            <legend>${g.message(code: "default.create.label", args: ["příspěvek"])}</legend>
            <g:render template="form" model="[entryInstance: entryInstance]"/>
        </fieldset>
        <input value="Uložit" type="submit" class="btn btn-primary"/>
    </g:form>
</div>

</body>
</html>