<%@ page import="forum.grails.Entry" %>

<bootstrap3:textArea model="${entryInstance}" field="text" id="entry-text"
                     labelName="Text" cols="4" rows="5"/>
<g:hiddenField type="hidden" name="topic.id" id="entry-topic-id" value="${entryInstance.topic.id}"/>

