<div class="form-group <g:hasErrors bean="${model}" field="${field}">has-error</g:hasErrors>">
    <label class="col-sm-2 control-label" for="${id}">${labelText}</label>

    <div class="col-sm-10">
        <g:textField name="${name}" id="${id}" value="${model ? model[field] : ''}" placeholder="${placeholder}"
                     class="form-control"/>
        <g:if test="${help}">
            <span class="help-block">${help}</span>
        </g:if>
        <g:hasErrors bean="${model}" field="${field}">
            <span class="help-block">
                <g:renderErrors bean="${model}" field="${field}"/>
            </span>
        </g:hasErrors>
    </div>
</div>
