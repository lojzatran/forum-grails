<div class="form-group <g:hasErrors bean="${model}" field="${field}">has-error</g:hasErrors>">
    <label class="col-sm-2 control-label" for="${id}">${labelText}</label>

    <div class="col-sm-10">
        <select multiple name="tagList" id="tagList" style="display: none;">
            <g:each in="${model[field]}" var="${tag}">
                <option selected="selected" value="${tag.id}" id="${tag.name}">${tag.name}</option>
            </g:each>
        </select>
        <select multiple data-role="tagsinput">
            <g:each in="${model[field]}" var="${tag}">
                <option value="${tag.name}" id="${tag.name}-tagsinput">${tag.name}</option>
            </g:each>
        </select>

        <g:hasErrors bean="${model}" field="${field}">
            <span class="help-block">
                <g:renderErrors bean="${model}" field="${field}"/>
            </span>
        </g:hasErrors>
    </div>
</div>
