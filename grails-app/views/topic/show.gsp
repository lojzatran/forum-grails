<%--
  Created by IntelliJ IDEA.
  User: Lojza
  Date: 12.1.14
  Time: 10:25
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>${topicInstance.name}</title>
    <meta name="layout" content="main"/>
</head>

<body>

<h1>${topicInstance.name}</h1>

<sec:canEditItem item="${topicInstance}">
    <p>
        <g:link action="edit" id="${topicInstance.id}">
            Editovat téma
        </g:link>
    </p>
</sec:canEditItem>

<g:if test="${topicInstance.tagList}">
    <p>
        <g:each in="${topicInstance.tagList}" var="tag">
            <g:link controller="topic" action="findByTag" params="[tagName: tag.name]"
                    class="label label-primary">
                ${tag.name}
            </g:link>
        </g:each>
    </p>
</g:if>

<div class="well">
    ${topicInstance.description}
</div>

<g:each in="${topicInstance.entryList}" var="entry">

    <div class="panel panel-default">
        <div class="panel-heading">
            Autor: ${entry.author.username} (dne ${entry.dateCreated})

            <sec:canEditItem item="${entry}">
                <div class="pull-right">

                    <g:form method="delete" onsubmit="return confirm('Jste si jistý?');"
                            controller="entry" action="delete" id="${entry.id}">
                        <g:link class="btn btn-primary" controller="entry"
                                action="edit" id="${entry.id}">Editovat</g:link>
                        <input type="submit" class="btn btn-danger" value="&times;"/>
                    </g:form>
                </div>
            </sec:canEditItem>

        </div>

        <div class="panel-body">
            ${entry.text}
        </div>
    </div>
</g:each>

<p>
    <sec:ifLoggedIn>
        <g:link controller="entry" action="create" params="['topic.id': topicInstance.id]">
            Odpovědět
        </g:link>
    </sec:ifLoggedIn>
    <sec:ifNotLoggedIn>
        Pokud chcete odpovědět, <g:link controller="login">přihlašte se.</g:link>
    </sec:ifNotLoggedIn>
</p>

</body>
</html>