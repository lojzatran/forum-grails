<%--
  Created by IntelliJ IDEA.
  User: Lojza
  Date: 18.4.14
  Time: 22:53
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Seznam témat</title>
    <meta name="layout" content="main"/>
</head>

<body>

<h1>Seznam témat</h1>

<ul>
    <g:each in="${topicList}" var="topic">
        <li>
            <g:link action="show" id="${topic.id}">${topic.name}</g:link>  (počet příspěvků: ${topic.entryList.size()})
        </li>
    </g:each>
</ul>

</body>
</html>