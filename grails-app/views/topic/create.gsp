<%--
  Created by IntelliJ IDEA.
  User: Lojza
  Date: 18.1.14
  Time: 11:16
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Nové téma</title>
    <meta name="layout" content="main"/>
    <r:require module="bootstrap-tagsinput"/>
</head>

<body>
<g:render template="form" model="[topicInstance: topicInstance, tagList: tagList]"/>
</body>
</html>