<%--
  Created by IntelliJ IDEA.
  User: Lojza
  Date: 18.1.14
  Time: 11:16
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>${topicInstance.name} - Editace</title>
    <meta name="layout" content="main"/>
    <r:require module="bootstrap-tagsinput"/>
</head>

<body>
<div class="row">
    <g:render template="form" model="[topicInstance: topicInstance, tagList: tagList]"/>
</div>

<sec:canEditItem item="${topicInstance}">
    <div>
        <g:form method="delete" onclick="return confirm('Jste si jistý?');" controller="topic" action="delete"
                resource="${topicInstance}">
            <input type="submit" value="Smazat" class="btn btn-danger"/>
        </g:form>
    </div>
</sec:canEditItem>
</body>
</html>