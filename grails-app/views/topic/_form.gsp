<div class="col-md-10">
    <g:hasErrors bean="${topicInstance}">
        <span class="help-block">
            <g:renderErrors bean="${topicInstance}"/>
        </span>
    </g:hasErrors>

    <g:form method="${params.action == 'create' ? "post" : "put"}" resource="${topicInstance}">
        <fieldset>
            <legend>${topicInstance ? topicInstance.name : g.message(code: "default.create.label", args: ["téma"])}</legend>

            <g:if test="${topicInstance.id}">
                <g:hiddenField name="version" value="${topicInstance.version}"/>
                <g:hiddenField name="id" value="${topicInstance.id}"/>
            </g:if>
            <bootstrap3:textField model="${topicInstance}" field="name" id="topic-name" labelName="Název"/>
            <bootstrap3:textArea model="${topicInstance}" field="description" id="topic-description"
                                 labelName="Popis tématu"
                                 cols="4" rows="5"/>
            <bootstrap3:tags model="${topicInstance}" field="tagList" id="topic-tags" labelName="Tagy"/>

        </fieldset>

        <g:if test="${params.action == 'create'}">
            <g:actionSubmit value="Uložit" action="save" type="submit" class="btn btn-primary"/>
        </g:if>
        <g:else>
            <g:actionSubmit value="Aktualizovat" action="update" type="submit" class="btn btn-primary"/>
        </g:else>
    </g:form>
</div>

<div class="col-md-2">
    <div class="panel panel-primary pull-right">
        <div class="panel-heading">Tagy</div>

        <div class="panel-body">
            <g:each in="${tagList}" var="tag">
                <p>${tag.name}</p>
            </g:each>
        </div>
    </div>
</div>