<%--
  Created by IntelliJ IDEA.
  User: Lojza
  Date: 26.1.14
  Time: 13:17
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Upravit profil</title>
    <meta name="layout" content="main"/>
</head>

<body>

<div>
    <g:form action="update" method="put" response="${profileCo}">
        <fieldset>
            <legend>${session.user.username} profil</legend>

            <bootstrap3:textField model="${profileCo}" field="email" labelName="Email"/>
            <bootstrap3:textField model="${profileCo}" field="firstName" labelName="Jméno"/>
            <bootstrap3:textField model="${profileCo}" field="lastName" labelName="Příjmení"/>
            <bootstrap3:passwordField model="${profileCo}" field="password" labelName="Heslo"/>
            <bootstrap3:passwordField model="${profileCo}" field="passwordAgain" labelName="Potvrďte heslo"/>
            <bootstrap3:textArea model="${profileCo}" field="signature" labelName="Podpis"/>

            <g:submitButton name="user-update-submit" type="submit" class="btn btn-primary" value="Aktualizuj"/>

        </fieldset>
    </g:form>
</div>

</body>
</html>