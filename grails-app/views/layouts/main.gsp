<!DOCTYPE html>

<html>
<head>
    <title><g:layoutTitle default="Fórum"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <g:layoutHead/>
    <r:require module="bootstrap"/>
    <r:layoutResources/>
</head>

<body>
<div class="container">
    <div class="row">

        <nav class="navbar navbar-default" role="navigation">

            <div class="navbar-header">
                <g:link url="/" class="navbar-brand">Fórum</g:link>
            </div>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <g:if test="${session.user}">
                        <li>
                            <g:link controller="user" action="edit" elementId="user-profile-link">Můj profil</g:link>
                        </li>
                        <li>
                            <g:link controller="login" action="logout">Logout</g:link>
                        </li>
                        <g:if test="${session.user.isAdmin()}">
                            <li>
                                <g:link controller="admin" elementId="admin-menu-link">Admin menu</g:link>
                            </li>
                        </g:if>
                    </g:if>
                    <g:else>
                        <li>
                            <g:link controller="signUp">Registrace</g:link>
                        </li>
                        <li>
                            <g:link controller="login">
                                <g:message code="login.action.label"/>
                            </g:link>
                        </li>
                    </g:else>
                </ul>
            </div>
        </nav>
    </div>

    <div class="row">
        <g:if test="${flash.containsKey("successMessage")}">
            <div class="alert alert-success">
                ${flash.successMessage}
            </div>
        </g:if>
        <g:elseif test="${flash.containsKey("errorMessage")}">
            <div class="alert alert-danger">
                ${flash.errorMessage}
            </div>
        </g:elseif>
    </div>

    <g:layoutBody/>

    <r:layoutResources/>
</body>
</html>