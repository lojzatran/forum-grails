<%--
  Created by IntelliJ IDEA.
  User: Lojza
  Date: 18.4.14
  Time: 23:14
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Seznam uživatelů</title>
    <meta name="layout" content="main"/>
</head>

<body>

<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Uživ. jméno</th>
        <th>Jméno</th>
        <th>Příjmení</th>
        <th>Email</th>
        <th>Podpis</th>
        <th>Avatar</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${userList}" var="user">
        <tr>
            <td>${user.id}</td>
            <td>$${user.username}</td>
            <g:set var="profile" value="${user.profile}"/>
            <th>${profile.firstName}</th>
            <th>${profile.lastName}</th>
            <th>${profile.email}</th>
            <th>${profile.signature}</th>
        </tr>
    </g:each>
    </tbody>
</table>

</body>
</html>