<%--
  Created by IntelliJ IDEA.
  User: Lojza
  Date: 26.1.14
  Time: 10:24
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Nový účet</title>
    <meta name="layout" content="main"/>
</head>

<body>

<div>
    <g:form action="signUp" method="post">
        <fieldset>
            <legend>Vytvoření účtu</legend>

            <bootstrap3:textField model="${signUpCo}" field="username" labelName="Uživatelské jméno"/>
            <bootstrap3:textField model="${signUpCo}" field="email" labelName="Email"/>
            <bootstrap3:passwordField model="${signUpCo}" field="password" labelName="Heslo"/>
            <bootstrap3:passwordField model="${signUpCo}" field="passwordAgain" labelName="Potvrďte heslo"/>

            <g:submitButton name="signup-submit" class="btn btn-default" value="Registrovat"/>
        </fieldset>
    </g:form>
</div>

</body>
</html>