package forum.grails

import grails.transaction.Transactional
import org.springframework.transaction.annotation.Propagation

@Transactional
class HomeController {

    def index() {
        List<Topic> topicList = Topic.findAll()
        render view: "index", model: [topicList: topicList]
    }
}
