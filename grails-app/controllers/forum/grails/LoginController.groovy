package forum.grails

import grails.transaction.Transactional
import org.springframework.transaction.annotation.Propagation

@Transactional
class LoginController {
    static allowedMethods = [login: "POST"]

    def index() {}

    def login(LoginCO loginCo) {
        if (loginCo.hasErrors()) {
            render view: "index", model: [loginCo: loginCo]
        } else {
            User user = User.findByUsernameAndPassword(loginCo.username, loginCo.password)
            if (user) {
                session.user = user
                flash.successMessage = g.message(code: "user.login.success.message")
                redirect controller: "home"
            } else {
                flash.errorMessage = g.message(code: "user.not.found.message")
                redirect action: "index"
            }
        }
    }

    def logout() {
        session.invalidate()
        flash.successMessage = g.message(code: "user.logout.success.message")
        redirect(controller: "home")
    }
}

class LoginCO {
    String username
    String password

    static constraints = {
        username(blank: false)
        password(blank: false, minSize: 3)
    }
}
