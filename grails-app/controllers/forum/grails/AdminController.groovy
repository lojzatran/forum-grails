package forum.grails

class AdminController {

    def index() {
        redirect action: "listUsers"
    }

    def listUsers() {
        List<User> userList = User.findAll()
        render view: "list", model: [userList: userList]
    }
}
