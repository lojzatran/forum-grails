package forum.grails

import grails.transaction.Transactional
import grails.validation.Validateable
import org.springframework.transaction.annotation.Propagation
import org.springframework.validation.ObjectError

import static org.springframework.http.HttpStatus.*

@Transactional
class SignUpController {
    static allowedMethods = [signUp: "POST"]

    def index(SignUpCO signUpCo) {
        respond signUpCo
    }

    @Transactional
    def signUp(SignUpCO signUpCo) {
        if (signUpCo.hasErrors()) {
            forward action: "index", model: [signUpCo: signUpCo]
            return
        }

        User user = signUpCo.getUserDomain()
        user.save(flush: true)

        sendRegistrationEmail(user)

        request.withFormat {
            form {
                flash.successMessage = message(code: 'user.registered.message')
                redirect controller: "login"
            }
            '*' { respond user, [status: CREATED] }
        }
    }


    private void sendRegistrationEmail(User user) {
        sendMail {
            from "Forum <noreply@email.com>"
            to user.profile.email
            subject "Registrace"
            html '<html>Vítáme vás v našem diskuzním fóru.</html>'
        }
    }
}

@Validateable
class SignUpCO {
    String username
    String password
    String passwordAgain
    String email

    static constraints = {
        username blank: false, validator: { val, obj ->
            boolean isUnique = false
            User.withNewTransaction {
                isUnique = User.countByUsername(val) == 0
            }
            if (!isUnique) {
                ["user.signup.username.not.unique", "Uživatelské jméno již existuje!"]
            }
        }
        password minSize: 3, validator: { val, obj ->
            if (val != obj.passwordAgain) {
                ["user.signup.password.not.matched", "Hesla se neshodují"]
            }
        }
        email email: true, blank: false, validator: { val, obj ->
            boolean isUnique = false
            Profile.withNewTransaction {
                isUnique = Profile.countByEmail(val) == 0
            }
            if (!isUnique) {
                ["user.signup.email.not.unique", "Email již existuje!"]
            }
        }
    }

    User getUserDomain() {
        User user = new User(username: this.username, password: this.password,
                role: Role.ROLE_USER, profile: new Profile(email: this.email))
        return user
    }
}