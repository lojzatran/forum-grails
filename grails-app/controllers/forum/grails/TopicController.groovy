package forum.grails

import org.springframework.transaction.annotation.Propagation

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional
class TopicController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
        redirect controller: "home", action: "index"
    }

    def show(Topic topicInstance) {
        respond topicInstance
    }

    def create() {
        List<Tag> tagList = Tag.findAll()
        respond new Topic(params), model: [tagList: tagList]
    }

    def findByTag(String tagName) {
        List<Topic> topicList = Topic.createCriteria().list {
            tagList {
                eq("name", tagName)
            }
        }

        render model: [topicList: topicList], view: "list"
    }

    @Transactional
    def save(Topic topicInstance) {
        if (topicInstance == null) {
            notFound()
            return
        }

        setAuthor(topicInstance)

        if (!topicInstance.validate()) {
            List<Tag> tagList = Tag.findAll()
            respond topicInstance.errors, model: [tagList: tagList], view: 'create'
            return
        }

        topicInstance.save(flush: true)

        request.withFormat {
            form {
                flash.successMessage = message(code: 'default.created.message', args: [message(code: 'topicInstance' +
                        '.label', default: 'Topic'), topicInstance.id])
                redirect topicInstance
            }
            '*' { respond topicInstance, [status: CREATED] }
        }
    }

    def edit(Topic topicInstance) {
        if (canEdit(topicInstance)) {
            List<Tag> tagList = Tag.findAll()
            respond topicInstance, model: [tagList: tagList]
        } else {
            flash.errorMessage = "Nemáte oprávnění editovat toto téma!"
            redirect action: "show", id: topicInstance.id
        }
    }

    @Transactional
    def update(TopicCO topicCO) {
        if (!topicCO) {
            render status: BAD_REQUEST
            return
        }

        Topic topicInstance = bindTopic(topicCO)

        if (topicInstance.hasErrors()) {
            respond topicInstance.errors, view: 'edit'
            return
        }

        topicInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Topic.label', default: 'Topic'), topicInstance.id])
                redirect controller: "home", action: "index"
            }
            '*' { respond topicInstance, [status: OK] }
        }
    }

    private Topic bindTopic(TopicCO topicCO) {
        Topic topic = Topic.get(topicCO.id)
        if (canEdit(topic)) {
            topic.properties['name', 'description'] = topicCO.properties
            Set tagIds = topic.tagList.collect { it.id }.toSet()
            def tagsToAdd = topicCO.tagList - tagIds
            tagsToAdd?.each { tagId ->
                if (!topic.tagList.any { it.id == tagId }) {
                    Tag tag = Tag.get(tagId)
                    topic.addToTagList(tag)
                }
            }

            def tagsToRemove = tagIds - topicCO.tagList
            tagsToRemove?.each { tagId ->
                topic.removeFromTagList(topic.tagList.find { it.id == tagId })
            }
        } else {
            topic.errors.reject("topic.unauthorized.edit")
        }

        return topic
    }

    private boolean canEdit(Topic topic) {
        return session.user.equals(topic.author) || session.user.role == Role.ROLE_ADMIN
    }

    @Transactional
    def delete(Topic topicInstance) {

        if (topicInstance == null) {
            notFound()
            return
        }

        topicInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Topic.label', default: 'Topic'), topicInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'topicInstance.label', default: 'Topic'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    protected void setAuthor(Topic topicInstance) {
        User user = User.get(session.user.id)
        topicInstance.setAuthor(user)
        user.addToMyTopics(topicInstance)
    }
}

class TopicCO {
    Long id

    String name
    String description

    Set<Long> tagList

    static constraints = {
        name blank: false
        description blank: false
    }
}