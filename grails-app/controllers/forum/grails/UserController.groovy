package forum.grails

import grails.validation.Validateable
import org.springframework.transaction.annotation.Propagation

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional
class UserController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
        redirect action: "edit"
    }

    def show(User userInstance) {
        respond userInstance
    }

    def edit() {
        User user = User.read(session.user.id as Long)
        Profile profile = user.profile
        ProfileCO profileCo = new ProfileCO(email: profile.email, firstName: profile.firstName,
                lastName: profile.lastName, signature: profile.signature)
        render view: "edit", model: [profileCo: profileCo]
    }

    @Transactional
    def update(ProfileCO profileCo) {
        if (profileCo == null) {
            notFound()
            return
        }

        String email = session.user.profile.email as String
        profileCo.validateEmail(email)

        if (profileCo.hasErrors()) {
            render view: "edit", model: [profileCo: profileCo]
            return
        }

        User userInstance = User.get(session.user.id as Long)

        profileCo.bindProfileDomain(userInstance)

        userInstance.save flush: true

        session.user = userInstance

        request.withFormat {
            form {
                flash.successMessage = message(code: 'default.updated.message', args: [message(code: 'User.label',
                        default: 'User'), userInstance.id])
                redirect action: "edit"
            }
            '*' { respond userInstance, [status: OK] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.errorMessage = message(code: 'default.not.found.message', args: [message(code: 'userInstance' +
                        '.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}

@Validateable
class ProfileCO {
    String email
    String firstName
    String lastName
    String password
    String passwordAgain
    String signature

    static constraints = {
        email email: true, blank: false
        firstName blank: true, nullable: true
        lastName blank: true, nullable: true
        password blank: true, nullable: true, validator: { val, obj ->
            if (val) {
                if (val.size() < 3) {
                    return ["user.password.min.size", "Prosím zadejte alespoň 3 znaky."]
                }
                if (val != obj.passwordAgain) {
                    return ["user.signup.password.not.matched", "Hesla se neshodují"]
                }
            }
        }
        passwordAgain blank: true, nullable: true
        signature blank: true, nullable: true
    }

    void validateEmail(String oldEmail) {
        boolean isUnique = false
        if (this.email != oldEmail) {
            Profile.withNewTransaction {
                isUnique = Profile.countByEmail(this.email) == 0
            }
            if (!isUnique) {
                this.errors.rejectValue("email", "user.signup.email.nfot.unique", "Email již existuje!")
            }
        }
    }

    void bindProfileDomain(User user) {
        Profile profile = user.profile
        profile.email = this.email
        profile.firstName = this.firstName ?: profile.firstName
        profile.lastName = this.lastName ?: profile.lastName
        user.password = this.password ?: user.password
        profile.signature = this.signature ?: profile.signature
    }
}