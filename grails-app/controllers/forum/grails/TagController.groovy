package forum.grails

import org.springframework.transaction.annotation.Propagation

import javax.servlet.http.HttpServletResponse

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional
class TagController {

    def exists(String tagName) {
        Tag tag = Tag.findByName(tagName)
        if (tag == null) {
            if (session.user?.role == Role.ROLE_ADMIN) {
                tag = createNewTag(tagName)
                render status: CREATED, text: tag.id
            } else {
                render status: NOT_FOUND, text: "Tag neexistuje, prosím vytvořte první."
            }
        } else {
            render status: OK, text: tag.id
        }
    }

    @Transactional
    private Tag createNewTag(String name) {
        Tag tag = new Tag()
        tag.setName(name)
        tag.save()
    }
}
