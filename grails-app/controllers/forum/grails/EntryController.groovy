package forum.grails

import org.springframework.transaction.annotation.Propagation

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional
class EntryController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Entry.list(params), model: [entryInstanceCount: Entry.count()]
    }

    def show(Entry entryInstance) {
        Long topicId = entryInstance.topic.id
        redirect controller: "topic", action: "show", id: topicId
    }

    def create() {
        respond new Entry(params)
    }

    @Transactional
    def save(Entry entryInstance) {
        if (entryInstance == null) {
            notFound()
            return
        }

        setAssociations(entryInstance)

        if (!entryInstance.validate()) {
            respond entryInstance.errors, view: 'create'
            return
        }

        entryInstance.save flush: true

        Long topicId = entryInstance.topic.id
        request.withFormat {
            form {
                flash.successMessage = message(code: 'default.created.message', args: [message(code: 'entryInstance.label', default: 'Entry'), entryInstance.id])
                redirect controller: "topic", action: "show", id: topicId
            }
            '*' { respond entryInstance, [status: CREATED] }
        }
    }

    protected void setAssociations(Entry entryInstance) {
        User user = User.get(session.user.id)
        entryInstance.setAuthor(user)
        user.addToMyEntries(entryInstance)
        Topic topic = entryInstance.topic
        topic.addToEntryList(entryInstance)
    }

    def edit(Entry entryInstance) {
        respond entryInstance
    }

    @Transactional
    def update(Entry entryInstance) {
        if (entryInstance == null) {
            notFound()
            return
        }

        if (entryInstance.hasErrors()) {
            respond entryInstance.errors, view: 'edit'
            return
        }

        entryInstance.save flush: true

        request.withFormat {
            form {
                flash.successMessage = message(code: 'default.updated.message', args: [message(code: 'Entry.label', default: 'Entry'), entryInstance.id])
                redirect entryInstance
            }
            '*' { respond entryInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Entry entryInstance) {

        if (entryInstance == null) {
            notFound()
            return
        }

        Long topicId = entryInstance.topic.id
        entryInstance.topic.removeFromEntryList(entryInstance)
        entryInstance.delete flush: true

        request.withFormat {
            form {
                flash.successMessage = message(code: 'default.deleted.message', args: [message(code: 'Entry.label',
                        default: 'Entry'), entryInstance.id])
                redirect controller: "topic", action: "show", id: topicId, method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.errorMessage = message(code: 'default.not.found.message', args: [message(code: 'entryInstance' +
                        '.label', default: 'Entry'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}

class EntryCO {
    String text

    static constraints = {
        text blank: false
    }
}
