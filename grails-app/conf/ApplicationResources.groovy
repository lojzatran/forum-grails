modules = {
    application {
        resource url: 'js/application.js'
    }

    bootstrap {
        dependsOn 'jquery'

        resource url: 'libs/bootstrap-3.0.3/css/bootstrap-theme.css'
        resource url: 'libs/bootstrap-3.0.3/css/bootstrap.css'
        resource url: 'libs/bootstrap-3.0.3/js/bootstrap.js'
    }

    'bootstrap-tagsinput' {
        dependsOn 'bootstrap'

        resource url: 'libs/bootstrap-tagsinput/bootstrap-tagsinput.css'
        resource url: 'libs/bootstrap-tagsinput/bootstrap-tagsinput.js'
        resource url: 'js/tags.js'
    }
}