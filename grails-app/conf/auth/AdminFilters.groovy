package auth

import forum.grails.Role

import javax.servlet.http.HttpServletResponse

class AdminFilters {

    def dependsOn = [SecurityFilters]

    def filters = {
        adminUrl(controller: 'admin', action: '*') {
            before = {
                if (session.user.role != Role.ROLE_ADMIN) {
                    return response.sendError(HttpServletResponse.SC_NOT_FOUND)
                }
            }
        }
    }
}
