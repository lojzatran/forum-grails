package auth

import forum.grails.Role
import forum.grails.User

class UserFilters {

    def dependsOn = [SecurityFilters]

    def filters = {
        userUrl(controller: 'user', action: '*') {
            before = {
                if (session.user.role != Role.ROLE_USER &&
                        session.user.role != Role.ROLE_ADMIN) {
                    redirect(controller: 'login')
                    return false
                }
            }
        }
    }
}
