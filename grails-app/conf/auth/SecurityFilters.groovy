package auth

class SecurityFilters {

    def filters = {
        loginCheck(action: 'login|index|show|signUp', invert: true) {
            before = {
                if (!session.user) {
                    redirect(controller: 'login')
                    return false
                }
            }
        }
    }
}
