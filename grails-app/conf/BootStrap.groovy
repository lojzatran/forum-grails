import forum.grails.Entry
import forum.grails.Profile
import forum.grails.Role
import forum.grails.Tag
import forum.grails.Topic
import forum.grails.User
import utils.Faker

class BootStrap {

    def init = { servletContext ->
        String adminStr = "admin"
        User admin = new User(username: adminStr, password: adminStr, role: Role.ROLE_ADMIN,
                profile: new Profile(firstName: "firstname${adminStr}", lastName: "lastname${adminStr}",
                        email: "${adminStr}@email.com", signature: "${adminStr}signature"))
        admin.save()
        String userStr = "user"
        User user = new User(username: userStr, password: userStr, profile:
                new Profile(firstName: "firstname${userStr}", lastName: "lastname${userStr}",
                        email: "${userStr}@email.com", signature: "${userStr}signature"))
        user.save()

        List<Tag> tagList = Faker.getRandomWordsAsArray(5).collect { tagName ->
            Tag tag = new Tag(name: tagName)
            tag.save()
        }
        (0..100).each { int i ->
            Topic topic = new Topic(name: "Testovací téma ${i}", description: Faker.getRandomWords(),
                    author: admin)
            Tag tag = tagList[i % 5]
            tag.addToTopicList(topic)
            topic.addToTagList(tag)
            topic.save()
            Faker.generateRandomNumberUpTo(100).each {
                Entry entry = new Entry(text: Faker.getRandomWords(), author: admin, topic: topic)
                entry.save()
            }
        }

    }

    def destroy = {
    }
}
