package tracker

class UserTrackerFilters {

    def filters = {
        all(controller: '*', action: '*') {
            before = {
                log.info("Pripojil se na adresu ${request.forwardURI} uzivatel s IP ${request.remoteAddr}")
            }
        }
    }
}
