$(document).ready(function () {
    var tagsInput = $('select[data-role="tagsinput"]');
    var options = $("#tagList");

    tagsInput.on('itemRemoved', function (event) {
        var itemName = event.item;
        console.log(itemName);
        console.log(event);
        $('#' + itemName).remove();
    });
    tagsInput.on('itemAdded', function (event) {
        var that = this;
        var itemName = event.item;
        $.ajax({
            url: '/tag/exists',
            type: 'POST',
            data: {
                tagName: event.item
            },
            success: function (data) {
                var newOption = '<option value="' + data + '" id="' + itemName + '" selected="selected">' + itemName + '</option>';
                options.append(newOption);
            },
            error: function (data) {
                $(that).tagsinput('remove', itemName);
            }
        });
    });
});